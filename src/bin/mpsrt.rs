use {
    mangaplus_parser::reader::*,
    rayon::prelude::*,
    std::{fs, io::Result as R, path::*},
};
fn main() -> R<()> {
    fs::read_dir("pb")?.par_bridge().try_for_each(|r| {
        let f = r?;
        if let Some(f_name) = f.path().file_name() {
            match f.file_type() {
                Ok(t) if t.is_file() => {
                    if let Ok(b) = fs::read(f.path()) {
                        if let mod_Response::OneOfdata::success(r) =
                            mangaplus_parser::parse_mangaplus_proto(&b).unwrap().data
                        {
                            if let mod_SuccessResult::OneOfdata::manga_viewer(m) = r.data {
                                let name =
                                    format!("{} ({})", m.title_name.replace("/", "_"), m.title_id);
                                let path = PathBuf::from("pb").join(name);
                                fs::create_dir_all(&path)?;
                                fs::rename(f.path(), path.join(&f_name))?;
                                println!(
                                    "MV {} -> {}",
                                    f.path().display(),
                                    path.join(f_name).display()
                                );
                                return Ok(());
                            }
                        }
                    }
                    println!("RM {}", f.path().display());
                    fs::remove_file(f.path())
                }
                Ok(_) => Ok(()),
                Err(e) => Err(e),
            }
        } else {
            Ok(())
        }
    })
}
