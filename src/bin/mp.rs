use {
    futures::stream::{iter, StreamExt as _, TryStreamExt as _},
    mangaplus_parser::reader::*,
    quick_protobuf::errors::Error as Qp,
    reqwest::Client,
    std::{env::args, io::Error as Io, mem::transmute, path::*, time::Instant},
    tokio::{
        fs,
        io::{AsyncWriteExt, BufWriter},
        //stream::{iter, StreamExt as _},
    },
};
#[tokio::main]
async fn main() -> Result<(), E> {
    let client = Client::builder()
        .http2_prior_knowledge()
        .no_trust_dns()
        .use_rustls_tls()
        .build()?;
    let _ = iter(args().skip(1))
        .map(|arg| {
            let client = Client::clone(&client);
            async move {
                let file = fs::read(&arg).await?;
                let mut inner = vec![];
                struct Data {
                    name: PathBuf,
                    key: Vec<u8>,
                    url: String,
                }
                if let mod_Response::OneOfdata::success(r) =
                    mangaplus_parser::parse_mangaplus_proto(&file)?.data
                {
                    if let mod_SuccessResult::OneOfdata::manga_viewer(m) = r.data {
                        let name = m.title_name.replace("/", "_");
                        let ch = PathBuf::from(&name).join(format!(
                            "(id{}) Ch. {}",
                            m.chapter_id,
                            m.chapter_name.replace("/", "_")
                        ));
                        fs::create_dir_all(&ch).await?;
                        println!("CREATE {}", ch.display());
                        let mut idx = 0u64;
                        for page in m.pages {
                            if let mod_Page::OneOfdata::manga_page(p) = page.data {
                                idx += 1;
                                let name = ch.join(&format!("{:0>2}.jpg", idx));
                                let key = hex::decode(p.encryption_key.as_bytes())?;
                                let url = p.image_url.to_string();
                                inner.push(Data { name, key, url });
                            }
                        }
                    }
                };
                iter(inner)
                    .map(|Data { name, url, key }| {
                        let client = &client;
                        async move {
                            let r = client.get(&url).send().await?;

                            let bin_l = r.content_length().unwrap_or(0xffff);
                            println!("CREATE {} = {}", name.display(), bin_l);
                            let mut file = fs::File::create(name).await?;
                            file.set_len(bin_l).await?;
                            let mut w = BufWriter::new(file);
                            let bin = r.bytes().await?;
                            let t0 = Instant::now();
                            if let (&[], &[ka, kb, kc, kd, ke, kf, kg, kh], &[]) =
                                unsafe { key.align_to::<u64>() }
                            {
                                if let (&[], b, ex) = unsafe { bin.align_to::<[u64; 8]>() } {
                                    for [va, vb, vc, vd, ve, vf, vg, vh] in b {
                                        let x = [
                                            ka ^ va,
                                            kb ^ vb,
                                            kc ^ vc,
                                            kd ^ vd,
                                            ke ^ ve,
                                            kf ^ vf,
                                            kg ^ vg,
                                            kh ^ vh,
                                        ];
                                        let x: [u8; 64] = unsafe { transmute(x) };
                                        w.write_all(&x).await?;
                                    }
                                    let v: Vec<_> = key
                                        .into_iter()
                                        .cycle()
                                        .zip(ex)
                                        .map(|(a, b)| a ^ b)
                                        .collect();
                                    w.write_all(&v).await?;
                                    w.flush().await?;
                                    println!("SIMD {} in {:?}", bin_l, t0.elapsed());
                                    return Ok(());
                                }
                            }
                            println!("BS");
                            let bs: Vec<_> = key
                                .into_iter()
                                .cycle()
                                .zip(bin.into_iter())
                                .map(|(a, b)| a ^ b)
                                .collect();
                            w.write_all(&bs).await?;
                            w.flush().await?;
                            Result::<(), E>::Ok(())
                        }
                    })
                    .buffer_unordered(8)
                    .try_collect()
                    .await?;
                println!("E");

                Result::<(), E>::Ok(())
            }
        })
        .buffer_unordered(8)
        .try_collect()
        .await?;
    Ok(())
}
#[derive(Debug, derive_more::Display, derive_more::From, derive_more::Error)]
enum E {
    Io(Io),
    Qp(Qp),
    H(hex::FromHexError),
    R(reqwest::Error),
}
